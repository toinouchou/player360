﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour{

    protected static T _onlyInstance = null;

    public static T Instance
    {
        get
        {
            if (_onlyInstance == null)
            {
                _onlyInstance = FindObjectOfType(typeof(T)) as T;
                if (_onlyInstance == null)
                    Debug.LogError("Could not locate a " + typeof(T).ToString() + " object. You have to have exactly one in the scene.");

            }
            return _onlyInstance;
        }
    }
}

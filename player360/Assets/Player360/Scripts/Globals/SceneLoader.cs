﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoSingleton<SceneLoader>
{

    private void Awake()
    {
        DownloadController.Instance.downloadStartedEvent += LoadLoadingScreen;
        DownloadController.Instance.callLoadPlayer += LoadPlayer;
        DownloadController.Instance.callBackDownloadSuccess += LoadVideoSelectionScene;
        DownloadController.Instance.callBackVideoFoundInMemory += LoadModeSelectionScreen;
        DownloadController.Instance.callBackDownloadError += LoadVideoSelectionScene;
        UIManager.Instance.splashScreenTimerOffEvent += InitializeScenes;
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HandleBackButton();
        }
    }

    void InitializeScenes()
    {
        SceneManager.LoadScene("UI", LoadSceneMode.Additive);
        Screen.sleepTimeout = 60;
    }

    void LoadVideoSelectionScene()
    {
        //Not very nice but efficient for now
        if (VRModeSwitcher.Instance != null)
            VRModeSwitcher.Instance.TurnOn2D();

        Screen.sleepTimeout = 60;
        SceneManager.LoadScene("UI");
    }

    void LoadLoadingScreen()
    {
        Debug.Log("Loading video downloading screen");
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        SceneManager.LoadScene("Loading");
    }

    void LoadModeSelectionScreen()
    {
        //Debug.Log("Loading Mode Selection screen");
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        SceneManager.LoadScene("ModeSelection");
    }

    void LoadPlayer()
    {
        Debug.Log("Loading Player360 scene");
        
        SceneManager.LoadScene("Player360");
    }

    public void HandleBackButton()
    {
        //Go back to selection screen if playing a video or selecting mode. Quit if on selection scene menu
        if (SceneManager.GetActiveScene().name == "UI" || SceneManager.GetActiveScene().buildIndex == 0)
        {
            Application.Quit();
        }
        else
        {
            LoadVideoSelectionScene();
        }
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("Level Loaded " + scene.name);

        if (scene.name == "Player360")
        {
            VideoPlayerWrapper.Instance.videoFinishedEvent += LoadVideoSelectionScene;
        }

        if(scene.name == "UI")
        {
        }
        /*if (scene.name == "Loading" || scene.name == "ModeSelection")*/
            
    }
}

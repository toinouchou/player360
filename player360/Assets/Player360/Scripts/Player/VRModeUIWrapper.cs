﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VRModeEventSystemWrapper : MonoBehaviour {

    public Canvas FlatModeCanvas;

	// Use this for initialization
	void Start () {
        HandleVRMode();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void HandleVRMode()
    {
        if(VRModeSwitcher.Instance.currentMode == VRModeSwitcher.Mode.VR)
        {
            GetComponent<StandaloneInputModule>().enabled = false;
            GetComponent<GvrPointerInputModule>().enabled = true;
            if(FlatModeCanvas != null)FlatModeCanvas.gameObject.SetActive(false);
        }
        else
        {
            GetComponent<StandaloneInputModule>().enabled = true;
            GetComponent<GvrPointerInputModule>().enabled = false;
            if (FlatModeCanvas != null) FlatModeCanvas.gameObject.SetActive(true);
        }
    }
}

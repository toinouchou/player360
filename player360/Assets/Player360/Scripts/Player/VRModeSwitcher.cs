﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class VRModeSwitcher : MonoSingleton<VRModeSwitcher> {

    private Coroutine _switchRoutine;
    private bool switchingMode = false;

    public enum Mode {flat, VR};
    public Mode currentMode = Mode.flat;
    public GlobalEvents.stringEvent modeSelected;
    public GlobalEvents.voidEvent modeLoaded;


    private void Awake()
    {
        modeLoaded += DownloadController.Instance.OnModeLoaded;
        modeSelected += DownloadController.Instance.OnModeSelected;
    }

    public void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    #region public calls
    public void PrepareForFlat()
    {
        currentMode = Mode.flat;
        if (modeSelected != null)
            modeSelected("flat");

        ApplyCurrentMode();
    }

    public void PrepareForVR()
    {
        Debug.Log("Preparing VR mode");
        currentMode = Mode.VR;
        if (modeSelected != null)
            modeSelected("VR");

        ApplyCurrentMode();
    }

    public void ApplyCurrentMode()
    {
        if (currentMode == Mode.flat)
            TurnOn2D();
        else if (currentMode == Mode.VR)
            TurnOnVR();
    }

    public void ToggleMode()
    {

        if (switchingMode)
            return;

        switchingMode = true;
        if (currentMode == Mode.flat)
            TurnOnVR();
        else if(currentMode == Mode.VR)
            TurnOn2D();
    }

    public void TurnOnVR()
    {
        Debug.Log("Turning On VR mode");
        if (switchingMode)
            return;

        switchingMode = true;

        if (_switchRoutine != null)
            StopCoroutine(_switchRoutine);

        _switchRoutine = StartCoroutine(SwitchToVR());
    }

    public void TurnOn2D()
    {
        if (switchingMode)
            return;

        switchingMode = true;

        if (_switchRoutine != null)
            StopCoroutine(_switchRoutine);

        _switchRoutine = StartCoroutine(SwitchTo2D());
    }
    #endregion





    #region VRMode
    // Call via `StartCoroutine(SwitchToVR())` from your code. Or, use
    // `yield SwitchToVR()` if calling from inside another coroutine.
    IEnumerator SwitchToVR()
    {
        Debug.Log("Switching to VR mode");
        // Device names are lowercase, as returned by `XRSettings.supportedDevices`.
        string desiredDevice = "cardboard";

        // Some VR Devices do not support reloading when already active, see
        // https://docs.unity3d.com/ScriptReference/XR.XRSettings.LoadDeviceByName.html
        Debug.Log("Load cardboard device");
        if (string.Compare(XRSettings.loadedDeviceName, desiredDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(desiredDevice);

            // Must wait one frame after calling `XRSettings.LoadDeviceByName()`.
            yield return null;
            yield return null;
        }

        Debug.Log("Verify cardboard device is loaded");
        
        Debug.Log("Loaded device " + XRSettings.loadedDeviceName + ". Expected device " + desiredDevice);
        yield return null;
        

        yield return null;
        // Now it's ok to enable VR mode.
        Debug.Log("Enable cardboard device");
        XRSettings.enabled = true;
        ModeSwitchEnd();
    }
    #endregion

    #region 2DMode
    // Call via `StartCoroutine(SwitchTo2D())` from your code. Or, use
    // `yield SwitchTo2D()` if calling from inside another coroutine.
    IEnumerator SwitchTo2D()
    {
        // Empty string loads the "None" device.
        XRSettings.LoadDeviceByName("");

        // Must wait one frame after calling `XRSettings.LoadDeviceByName()`.
        yield return null;
        yield return null;
        yield return null;

        // Not needed, since loading the None (`""`) device takes care of this.
        // XRSettings.enabled = false;

        // Restore 2D camera settings.
        ResetCameras();
        ModeSwitchEnd();
    }

    // Resets camera transform and settings on all enabled eye cameras.
    void ResetCameras()
    {
        // Camera looping logic copied from GvrEditorEmulator.cs
        for (int i = 0; i < Camera.allCameras.Length; i++)
        {
            Camera cam = Camera.allCameras[i];
            if (cam.enabled && cam.stereoTargetEye != StereoTargetEyeMask.None)
            {

                // Reset local position.
                // Only required if you change the camera's local position while in 2D mode.
                cam.transform.localPosition = Vector3.zero;

                // Reset local rotation.
                // Only required if you change the camera's local rotation while in 2D mode.
                cam.transform.localRotation = Quaternion.identity;

                // No longer needed, see issue github.com/googlevr/gvr-unity-sdk/issues/628.
                // cam.ResetAspect();

                // No need to reset `fieldOfView`, since it's reset automatically.
            }
        }
    }

    #endregion

    private void ModeSwitchEnd()
    {
        switchingMode = false;
        if (modeLoaded != null)
            modeLoaded();
    }
}

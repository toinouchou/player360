﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR;

public class VideoPlayerWrapper : MonoSingleton<VideoPlayerWrapper> {

    public VideoPlayer Player360;
    public string currentVideoURL;

    private Coroutine waitRoutine;

    public GlobalEvents.voidEvent videoFinishedEvent;


    private void Awake()
    {
        DownloadController.Instance.getSavedVideoURL += SetupVideo;
        videoFinishedEvent += VRModeSwitcher.Instance.TurnOn2D;
        Player360.loopPointReached += VideoFinished;
    }
    

    private void Start()
    {
    }

    private void OnDestroy()
    {
        DownloadController.Instance.getSavedVideoURL -= SetupVideo;
    }

    public void SetupVideo(string URL)
    {
        currentVideoURL = URL;

        Player360.url = URL;

        if (waitRoutine != null)
            StopCoroutine(waitRoutine);

        waitRoutine = StartCoroutine(WaitPlayerReady());
    }

    void VideoFinished(VideoPlayer source)
    {
        if (videoFinishedEvent != null)
            videoFinishedEvent();
    }


    IEnumerator WaitPlayerReady()
    {
        Player360.Prepare();

        while(Player360.isPrepared != true)
        {
            //Debug.Log("Player not ready.");
            yield return new WaitForSeconds(0.1f);
        }

        if (VRModeSwitcher.Instance.currentMode == VRModeSwitcher.Mode.VR)
        {
            yield return new WaitForSeconds(1.5f);
            //GvrCardboardHelpers.Recenter();
        }
        
        Player360.Play();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DownloadController : MonoSingleton<DownloadController> {

    public string currentURL;
    public string currentVideo;

    #region Event
    public GlobalEvents.floatEvent downloadProgressEvent;
    public GlobalEvents.voidEvent callBackDownloadError, callBackDownloadSuccess, downloadStartedEvent, callBackVideoFoundInMemory, callLoadPlayer;
    public GlobalEvents.stringEvent getSavedVideoURL, videoDeleted, videoNotDeleted;
    #endregion

    private string baseURL = "http://maestra360.visages360.com/REPDOM/getvideo.php?video=", videoExtension = ".mp4";
    public bool useExtension = true, modeIsSelected = false, modeIsLoaded = false;
    private Coroutine downloadRoutine, waitModeSelectionRoutine;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Download(string videoName)
    {
        if (downloadRoutine != null)
            return;

        PrepareDownload();
        currentVideo = videoName;
        downloadRoutine = StartCoroutine(DownloadRoutine(BuildVideoURL(videoName)));

    }

    public void Delete(string videoName)
    {
        string path;

        path = BuildVideoMemoryPath(videoName);

        if(File.Exists(path))
        {
            File.Delete(path);
            Debug.Log("Deleting video " + videoName + " at path : " + path);
            if (videoDeleted != null)
                videoDeleted(videoName);
        }
        else
        {
            Debug.Log("Video " + videoName + " was not found for deletion at path : " + path);
            if (videoNotDeleted != null)
                videoNotDeleted(videoName);
        }
    }

    public void LoadFromMemory(string videoName)
    {
        string path;

        path = BuildVideoMemoryPath(videoName);

        if (File.Exists(path))
        {
            Debug.Log("Found video " + videoName + " at path : " + path);
            Debug.Log("Start play");

            if (callBackVideoFoundInMemory != null)
                callBackVideoFoundInMemory();

            currentURL = path;

            FinishLoad();
        }
        else
        {
            Debug.Log("Video " + videoName + " was not found for loading at path : " + path);
        }
    }

    public void OnModeSelected(string mode)
    {
        modeIsSelected = true;
    }

    public void OnModeLoaded()
    {
        Debug.Log("Mode loaded successfuly");
        if (!modeIsSelected)
            return;

        modeIsLoaded = true;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Level Loaded " + scene.name);
        
        if(scene.name == "Player360")
        {
            if (getSavedVideoURL != null)
            {
                Debug.Log("Sending Level loaded callback");
                getSavedVideoURL(currentURL);
            }
        }

        if (scene.name == "UI"|| scene.name == "Loading" || scene.name == "ModeSelection")
        {
            modeIsSelected = false;
            modeIsLoaded = false;
        }
            

        if (scene.name == "UI")
        {
            StopAllCoroutines();
        }
        
    }

    IEnumerator DownloadRoutine(string URL)
    {
        currentURL = URL;
        WWW request = new WWW(URL);

        if(downloadStartedEvent != null)
            downloadStartedEvent();

        StartCoroutine(ShowProgress(request));

        yield return request;
       
        yield return true;

        if(request.error != null)
        {
            Debug.Log("Error from downloading request : " + request.error);
            Debug.Log("URL of request is : " + URL);
            if(callBackDownloadError != null)
                callBackDownloadError();

            yield break;
        }

       

        //Sauvegarde en mémoire
        yield return StartCoroutine(SaveToMemory(request.bytes));

        //Successful download
        if (callBackDownloadSuccess != null)
            callBackDownloadSuccess();


        FinishDownload();

    }

    IEnumerator WaitModeSelection()
    {
        while(!modeIsSelected || !modeIsLoaded)
        {
            yield return true;
        }

        Debug.Log("Mode selected and loaded. Loading player");
        yield return true;

        if (callLoadPlayer != null)
            callLoadPlayer();
    }

    IEnumerator ShowProgress(WWW request)
    {
        Debug.Log("Showing progress start up.");
        while (!request.isDone)
        {
            if (downloadProgressEvent != null)
                downloadProgressEvent(request.progress);

            //Debug.Log(string.Format("Downloaded {0:P1}", request.progress));
            yield return new WaitForSeconds(.1f);
        }

        /*foreach (KeyValuePair<string, string> kvp in request.responseHeaders)
        {
            Debug.Log("header key : " + kvp.Key + ", value : " + kvp.Value);
        }*/

        Debug.Log("Done");
    }

    IEnumerator SaveToMemory(byte[] data)
    {
        string write_path;

        SaveFolderExists();

#if UNITY_EDITOR
        write_path = Application.dataPath + "/VideosDownloaded/" + currentVideo;
        if(useExtension) write_path += videoExtension;
        Debug.Log("write path : " + write_path);
#elif UNITY_ANDROID
        write_path = Application.persistentDataPath + "/" + currentVideo;
        if(useExtension) write_path += videoExtension;
        Debug.Log("write path : " + write_path);
#elif UNITY_IOS
        write_path = Application.persistentDataPath + "/" + currentVideo +"_IOS";
        if(useExtension) write_path += videoExtension;
        Debug.Log("write path : " + write_path);
#endif

        File.WriteAllBytes(write_path, data);
        yield return true;

        currentURL = write_path;
    }

    private void PrepareDownload()
    {
        //FadeTOBlack, scene switch etc
       
    }

    private void FinishDownload()
    {
        downloadRoutine = null;
    }


    private void FinishLoad()
    {
        if (waitModeSelectionRoutine != null)
            StopCoroutine(waitModeSelectionRoutine);

        waitModeSelectionRoutine = StartCoroutine(WaitModeSelection());
    }

    private string BuildVideoURL(string videoName)
    {
        Debug.Log("baseURL :" + baseURL);
        string finalURL = string.Concat(baseURL, videoName);

        if (useExtension)
            finalURL = finalURL + videoExtension;

        return (finalURL);
    }

    private void SaveFolderExists()
    {
        string path;
#if UNITY_EDITOR
        path = Application.dataPath + "/VideosDownloaded";
        Debug.Log("Save folder path : " + path);
#elif UNITY_ANDROID
        path = Application.persistentDataPath;
        Debug.Log("write path : " + path);
#elif UNITY_IOS
        path = Application.persistentDataPath;
        Debug.Log("write path : " + path);
#endif

        if (!Directory.Exists(path))
        {
            Debug.Log("Save folder not found. Creating it.");
            Directory.CreateDirectory(path);
        }
    }

    private string BuildVideoMemoryPath(string videoName)
    {
        string path;
#if UNITY_EDITOR
        path = Application.dataPath + "/VideosDownloaded/" + videoName;
        if (useExtension) path = path + videoExtension;
#elif UNITY_ANDROID
        path = Application.persistentDataPath+ "/" + videoName;
        if (useExtension) path = path + videoExtension;
#elif UNITY_IOS
        path = Application.persistentDataPath+ "/" + videoName + "_IOS";
        if (useExtension) path = path + videoExtension; 
#endif
        return path;
    }
}   
    

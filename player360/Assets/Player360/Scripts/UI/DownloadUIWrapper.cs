﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadUIWrapper : MonoSingleton<DownloadUIWrapper> {

    public List<ButtonVideoWrapper> ButtonsList;
    public GlobalEvents.stringEvent downloadEvent, deleteEvent, loadFromMemoryEvent;

    private void Awake()
    {
        downloadEvent += DownloadController.Instance.Download;
        deleteEvent += DownloadController.Instance.Delete;
        loadFromMemoryEvent += DownloadController.Instance.LoadFromMemory;
    }

    private void Start()
    {
        DownloadController.Instance.videoDeleted += RefreshUI;
        DownloadController.Instance.videoNotDeleted += RefreshUI;
    }

    private void OnDestroy()
    {
        DownloadController.Instance.videoDeleted -= RefreshUI;
        DownloadController.Instance.videoNotDeleted -= RefreshUI;
    }

    public void RefreshUI(string video)
    {
        foreach(ButtonVideoWrapper Butt in ButtonsList)
        {
            if (video == Butt.VideoID) Butt.Refresh();
        }
    }

    public void CallDownload(string videoName)
    {
        if (downloadEvent != null)
            downloadEvent(videoName);
    }

    public void CallDelete(string videoName)
    {
        if (deleteEvent != null)
            deleteEvent(videoName);
    }

    public void CallLoadFromMemory(string videoName)
    {
        if (loadFromMemoryEvent != null)
            loadFromMemoryEvent(videoName);
    }

}

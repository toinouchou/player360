﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButtonUIWrapper : MonoBehaviour {

    GlobalEvents.voidEvent videoBackButtonPressed;

	// Use this for initialization
	void Start () {
        videoBackButtonPressed += SceneLoader.Instance.HandleBackButton;
	}
	
	public void BackButton()
    {
        if (videoBackButtonPressed != null)
            videoBackButtonPressed();
    }
}

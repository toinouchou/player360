﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelectionUIWrapper : MonoBehaviour {


    GlobalEvents.voidEvent VRModeEvent, flatModeEvent;

	// Use this for initialization
	void Awake () {
        VRModeEvent += VRModeSwitcher.Instance.PrepareForVR;
        flatModeEvent += VRModeSwitcher.Instance.PrepareForFlat;
	}
	
	public void callVRMode()
    {
        Debug.Log("VR mode button clicked");
        VRModeEvent();
    }

    public void callFlatMode()
    {
        flatModeEvent();
    }
}

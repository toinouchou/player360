﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ButtonVideoWrapper : MonoBehaviour {

    public GameObject PlayAndDeleteGroup;
    public GameObject DownloadButton;
    public string VideoID;

    private bool useExtension = true;
    private string videoExtension = ".mp4";

    private void Start()
    {
        VideoExists();
    }

    public void Refresh()
    {
        VideoExists();
    }

    private void VideoExists()
    {
        string path;
#if UNITY_EDITOR
        path = Application.dataPath + "/VideosDownloaded/" + VideoID;
        if (useExtension) path = path + videoExtension;
#elif UNITY_ANDROID
        path = Application.persistentDataPath+ "/" + VideoID;
        if (useExtension) path = path + videoExtension;
#elif UNITY_IOS
        path = Application.persistentDataPath+ "/" + VideoID + "_IOS";
        if (useExtension) path = path + videoExtension; 
#endif

        Debug.Log("Looking for video " + VideoID + " at path : " + path);
        if (File.Exists(path))
        {
            Debug.Log("Video "+ VideoID + " found in memory.");
            SetButtonState(true);
        }
        else {
            Debug.Log("Video " + VideoID + " not found.");
            SetButtonState(false);

        }
    }


    private void SetButtonState(bool videoInMemory)
    {
        PlayAndDeleteGroup.SetActive(videoInMemory);
        DownloadButton.SetActive(!videoInMemory);
    }
}

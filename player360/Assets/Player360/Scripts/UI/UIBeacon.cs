﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Needs to be improved with ID hierarchy. Check Merial project.
/// </summary>
public class UIBeacon : MonoBehaviour {

    public string ID;

    private void Awake()
    {
        UIManager.Instance.UIStateEvent += SetState;
    }

    private void OnDestroy()
    {
        UIManager.Instance.UIStateEvent -= SetState;
    }

    public void SetState(string state)
    {
        if (ID == state)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoWrapper : MonoBehaviour {

    public GameObject popup;
    
	// Use this for initialization
	void Start () {
        DisplayPopup(false);
	}
	
	public void DisplayPopup(bool state)
    {
        popup.SetActive(state);
    }

    public void OpenURL(string URL)
    {
        string httpURL = "http://";

        httpURL += URL;

        Application.OpenURL(httpURL);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadProgressWraper : MonoBehaviour {

    public Text progressText;
    public Slider progressBar;
    public float currentProgress;

	// Use this for initialization
	void Start () {
        UpdateProgress(0f);
        DownloadController.Instance.downloadProgressEvent += UpdateProgress;		
	}
	
	
	void UpdateProgress (float progress) {

        currentProgress = progress;
        //Debug.Log("Downloding progress : " + progress);
        progressText.text = string.Format("Téléchargement {0:P0}", progress);
        progressBar.value = progress;

    }

    private void OnDestroy()
    {
        DownloadController.Instance.downloadProgressEvent -= UpdateProgress;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoSingleton<UIManager> {

    public GlobalEvents.stringEvent UIStateEvent;
    public GlobalEvents.voidEvent splashScreenTimerOffEvent;
    public GameObject splashScreen;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(ShowSplashScreen());
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {

        if (scene.name == "Loading")
        {
            if (UIStateEvent != null)
            {
                UIStateEvent("ModeSelectionOn");
            }
            Debug.Log("Setting up mode selection callback");
            VRModeSwitcher.Instance.modeSelected += modeSelected;
        }

        if(scene.name == "UI")
        {
            Destroy(splashScreen);
        }
    }

    public void modeSelected(string mode)
    {
        if (UIStateEvent != null)
        {
            UIStateEvent("ModeSelectionOff");
        }
        VRModeSwitcher.Instance.modeSelected -= modeSelected;
    }

    IEnumerator ShowSplashScreen()
    {
        yield return new WaitForSeconds(2f);

        if (splashScreenTimerOffEvent != null)
            splashScreenTimerOffEvent();
        
    }

}
